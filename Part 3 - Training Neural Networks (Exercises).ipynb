{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Training Neural Networks\n",
    "\n",
    "The network we built in the previous part isn't so smart, it doesn't know anything about our handwritten digits. Neural networks with non-linear activations work like universal function approximators. There is some function that maps your input to the output. For example, images of handwritten digits to class probabilities. The power of neural networks is that we can train them to approximate this function, and basically any function given enough data and compute time.\n",
    "\n",
    "<img src=\"assets/function_approx.png\" width=500px>\n",
    "\n",
    "At first the network is naive, it doesn't know the function mapping the inputs to the outputs. We train the network by showing it examples of real data, then adjusting the network parameters such that it approximates this function.\n",
    "\n",
    "To find these parameters, we need to know how poorly the network is predicting the real outputs. For this we calculate a **loss function** (also called the cost), a measure of our prediction error. For example, the mean squared loss is often used in regression and binary classification problems\n",
    "\n",
    "$$\n",
    "\\large \\ell = \\frac{1}{2n}\\sum_i^n{\\left(y_i - \\hat{y}_i\\right)^2}\n",
    "$$\n",
    "\n",
    "where $n$ is the number of training examples, $y_i$ are the true labels, and $\\hat{y}_i$ are the predicted labels.\n",
    "\n",
    "By minimizing this loss with respect to the network parameters, we can find configurations where the loss is at a minimum and the network is able to predict the correct labels with high accuracy. We find this minimum using a process called **gradient descent**. The gradient is the slope of the loss function and points in the direction of fastest change. To get to the minimum in the least amount of time, we then want to follow the gradient (downwards). You can think of this like descending a mountain by following the steepest slope to the base.\n",
    "\n",
    "<img src='assets/gradient_descent.png' width=350px>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Backpropagation\n",
    "\n",
    "For single layer networks, gradient descent is straightforward to implement. However, it's more complicated for deeper, multilayer neural networks like the one we've built. Complicated enough that it took about 30 years before researchers figured out how to train multilayer networks.\n",
    "\n",
    "Training multilayer networks is done through **backpropagation** which is really just an application of the chain rule from calculus. It's easiest to understand if we convert a two layer network into a graph representation.\n",
    "\n",
    "<img src='assets/backprop_diagram.png' width=550px>\n",
    "\n",
    "In the forward pass through the network, our data and operations go from bottom to top here. We pass the input $x$ through a linear transformation $L_1$ with weights $W_1$ and biases $b_1$. The output then goes through the sigmoid operation $S$ and another linear transformation $L_2$. Finally we calculate the loss $\\ell$. We use the loss as a measure of how bad the network's predictions are. The goal then is to adjust the weights and biases to minimize the loss.\n",
    "\n",
    "To train the weights with gradient descent, we propagate the gradient of the loss backwards through the network. Each operation has some gradient between the inputs and outputs. As we send the gradients backwards, we multiply the incoming gradient with the gradient for the operation. Mathematically, this is really just calculating the gradient of the loss with respect to the weights using the chain rule.\n",
    "\n",
    "$$\n",
    "\\large \\frac{\\partial \\ell}{\\partial W_1} = \\frac{\\partial L_1}{\\partial W_1} \\frac{\\partial S}{\\partial L_1} \\frac{\\partial L_2}{\\partial S} \\frac{\\partial \\ell}{\\partial L_2}\n",
    "$$\n",
    "\n",
    "**Note:** I'm glossing over a few details here that require some knowledge of vector calculus, but they aren't necessary to understand what's going on.\n",
    "\n",
    "We update our weights using this gradient with some learning rate $\\alpha$. \n",
    "\n",
    "$$\n",
    "\\large W^\\prime_1 = W_1 - \\alpha \\frac{\\partial \\ell}{\\partial W_1}\n",
    "$$\n",
    "\n",
    "The learning rate $\\alpha$ is set such that the weight update steps are small enough that the iterative method settles in a minimum."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Losses in PyTorch\n",
    "\n",
    "Let's start by seeing how we calculate the loss with PyTorch. Through the `nn` module, PyTorch provides losses such as the cross-entropy loss (`nn.CrossEntropyLoss`). You'll usually see the loss assigned to `criterion`. As noted in the last part, with a classification problem such as MNIST, we're using the softmax function to predict class probabilities. With a softmax output, you want to use cross-entropy as the loss. To actually calculate the loss, you first define the criterion then pass in the output of your network and the correct labels.\n",
    "\n",
    "Something really important to note here. Looking at [the documentation for `nn.CrossEntropyLoss`](https://pytorch.org/docs/stable/nn.html#torch.nn.CrossEntropyLoss),\n",
    "\n",
    "> This criterion combines `nn.LogSoftmax()` and `nn.NLLLoss()` in one single class.\n",
    ">\n",
    "> The input is expected to contain scores for each class.\n",
    "\n",
    "This means we need to pass in the raw output of our network into the loss, not the output of the softmax function. This raw output is usually called the *logits* or *scores*. We use the logits because softmax gives you probabilities which will often be very close to zero or one but floating-point numbers can't accurately represent values near zero or one ([read more here](https://docs.python.org/3/tutorial/floatingpoint.html)). It's usually best to avoid doing calculations with probabilities, typically we use log-probabilities."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import torch\n",
    "from torch import nn\n",
    "import torch.nn.functional as F\n",
    "from torchvision import datasets, transforms\n",
    "\n",
    "# Define a transform to normalize the data\n",
    "transform = transforms.Compose([transforms.ToTensor(),\n",
    "                                transforms.Normalize((0.5,), (0.5,)),\n",
    "                              ])\n",
    "# Download and load the training data\n",
    "trainset = datasets.MNIST('~/.pytorch/MNIST_data/', download=True, train=True, transform=transform)\n",
    "trainloader = torch.utils.data.DataLoader(trainset, batch_size=64, shuffle=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Note\n",
    "If you haven't seen `nn.Sequential` yet, please finish the end of the Part 2 notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "tensor(2.2968, grad_fn=<NllLossBackward>)\n"
     ]
    }
   ],
   "source": [
    "# Build a feed-forward network\n",
    "model = nn.Sequential(nn.Linear(784, 128),\n",
    "                      nn.ReLU(),\n",
    "                      nn.Linear(128, 64),\n",
    "                      nn.ReLU(),\n",
    "                      nn.Linear(64, 10))\n",
    "\n",
    "# Define the loss\n",
    "criterion = nn.CrossEntropyLoss()\n",
    "\n",
    "# Get our data\n",
    "images, labels = next(iter(trainloader))\n",
    "# Flatten images\n",
    "images = images.view(images.shape[0], -1)\n",
    "\n",
    "# Forward pass, get our logits\n",
    "logits = model(images)\n",
    "# Calculate the loss with the logits and the labels\n",
    "loss = criterion(logits, labels)\n",
    "\n",
    "print(loss)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In my experience it's more convenient to build the model with a log-softmax output using `nn.LogSoftmax` or `F.log_softmax` ([documentation](https://pytorch.org/docs/stable/nn.html#torch.nn.LogSoftmax)). Then you can get the actual probabilities by taking the exponential `torch.exp(output)`. With a log-softmax output, you want to use the negative log likelihood loss, `nn.NLLLoss` ([documentation](https://pytorch.org/docs/stable/nn.html#torch.nn.NLLLoss)).\n",
    "\n",
    ">**Exercise:** Build a model that returns the log-softmax as the output and calculate the loss using the negative log likelihood loss. Note that for `nn.LogSoftmax` and `F.log_softmax` you'll need to set the `dim` keyword argument appropriately. `dim=0` calculates softmax across the rows, so each column sums to 1, while `dim=1` calculates across the columns so each row sums to 1. Think about what you want the output to be and choose `dim` appropriately."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "tensor(2.2911, grad_fn=<NllLossBackward>)\n"
     ]
    }
   ],
   "source": [
    "# TODO: Build a feed-forward network\n",
    "model = nn.Sequential(nn.Linear(784, 128),\n",
    "                     nn.ReLU(),\n",
    "                     nn.Linear(128,64),\n",
    "                     nn.ReLU(),\n",
    "                     nn.Linear(64,10),\n",
    "                     nn.LogSoftmax(dim=1))\n",
    "\n",
    "# TODO: Define the loss\n",
    "criterion = nn.NLLLoss()\n",
    "\n",
    "### Run this to check your work\n",
    "# Get our data\n",
    "images, labels = next(iter(trainloader))\n",
    "# Flatten images\n",
    "images = images.view(images.shape[0], -1)\n",
    "\n",
    "# Forward pass, get our logits\n",
    "logits = model(images)\n",
    "# Calculate the loss with the logits and the labels\n",
    "loss = criterion(logits, labels)\n",
    "\n",
    "print(loss)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Autograd\n",
    "\n",
    "Now that we know how to calculate a loss, how do we use it to perform backpropagation? Torch provides a module, `autograd`, for automatically calculating the gradients of tensors. We can use it to calculate the gradients of all our parameters with respect to the loss. Autograd works by keeping track of operations performed on tensors, then going backwards through those operations, calculating gradients along the way. To make sure PyTorch keeps track of operations on a tensor and calculates the gradients, you need to set `requires_grad = True` on a tensor. You can do this at creation with the `requires_grad` keyword, or at any time with `x.requires_grad_(True)`.\n",
    "\n",
    "You can turn off gradients for a block of code with the `torch.no_grad()` content:\n",
    "```python\n",
    "x = torch.zeros(1, requires_grad=True)\n",
    ">>> with torch.no_grad():\n",
    "...     y = x * 2\n",
    ">>> y.requires_grad\n",
    "False\n",
    "```\n",
    "\n",
    "Also, you can turn on or off gradients altogether with `torch.set_grad_enabled(True|False)`.\n",
    "\n",
    "The gradients are computed with respect to some variable `z` with `z.backward()`. This does a backward pass through the operations that created `z`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "tensor([[-1.7566,  0.0215],\n",
      "        [-0.3352, -0.6122]], requires_grad=True)\n"
     ]
    }
   ],
   "source": [
    "x = torch.randn(2,2, requires_grad=True)\n",
    "print(x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "tensor([[3.0856e+00, 4.6140e-04],\n",
      "        [1.1235e-01, 3.7479e-01]], grad_fn=<PowBackward0>)\n"
     ]
    }
   ],
   "source": [
    "y = x**2\n",
    "print(y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Below we can see the operation that created `y`, a power operation `PowBackward0`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<PowBackward0 object at 0x128c1e710>\n"
     ]
    }
   ],
   "source": [
    "## grad_fn shows the function that generated this variable\n",
    "print(y.grad_fn)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The autograd module keeps track of these operations and knows how to calculate the gradient for each one. In this way, it's able to calculate the gradients for a chain of operations, with respect to any one tensor. Let's reduce the tensor `y` to a scalar value, the mean."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "tensor(0.8933, grad_fn=<MeanBackward1>)\n"
     ]
    }
   ],
   "source": [
    "z = y.mean()\n",
    "print(z)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can check the gradients for `x` and `y` but they are empty currently."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "None\n"
     ]
    }
   ],
   "source": [
    "print(x.grad)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To calculate the gradients, you need to run the `.backward` method on a Variable, `z` for example. This will calculate the gradient for `z` with respect to `x`\n",
    "\n",
    "$$\n",
    "\\frac{\\partial z}{\\partial x} = \\frac{\\partial}{\\partial x}\\left[\\frac{1}{n}\\sum_i^n x_i^2\\right] = \\frac{x}{2}\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "tensor([[-0.8783,  0.0107],\n",
      "        [-0.1676, -0.3061]])\n",
      "tensor([[-0.8783,  0.0107],\n",
      "        [-0.1676, -0.3061]], grad_fn=<DivBackward0>)\n"
     ]
    }
   ],
   "source": [
    "z.backward()\n",
    "print(x.grad)\n",
    "print(x/2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These gradients calculations are particularly useful for neural networks. For training we need the gradients of the cost with respect to the weights. With PyTorch, we run data forward through the network to calculate the loss, then, go backwards to calculate the gradients with respect to the loss. Once we have the gradients we can make a gradient descent step. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Loss and Autograd together\n",
    "\n",
    "When we create a network with PyTorch, all of the parameters are initialized with `requires_grad = True`. This means that when we calculate the loss and call `loss.backward()`, the gradients for the parameters are calculated. These gradients are used to update the weights with gradient descent. Below you can see an example of calculating the gradients using a backwards pass."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Build a feed-forward network\n",
    "model = nn.Sequential(nn.Linear(784, 128),\n",
    "                      nn.ReLU(),\n",
    "                      nn.Linear(128, 64),\n",
    "                      nn.ReLU(),\n",
    "                      nn.Linear(64, 10),\n",
    "                      nn.LogSoftmax(dim=1))\n",
    "\n",
    "criterion = nn.NLLLoss()\n",
    "images, labels = next(iter(trainloader))\n",
    "images = images.view(images.shape[0], -1)\n",
    "\n",
    "logits = model(images)\n",
    "loss = criterion(logits, labels)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Before backward pass: \n",
      " None\n",
      "After backward pass: \n",
      " tensor([[-0.0029, -0.0029, -0.0029,  ..., -0.0029, -0.0029, -0.0029],\n",
      "        [ 0.0008,  0.0008,  0.0008,  ...,  0.0008,  0.0008,  0.0008],\n",
      "        [-0.0028, -0.0028, -0.0028,  ..., -0.0028, -0.0028, -0.0028],\n",
      "        ...,\n",
      "        [-0.0105, -0.0105, -0.0105,  ..., -0.0105, -0.0105, -0.0105],\n",
      "        [ 0.0028,  0.0028,  0.0028,  ...,  0.0028,  0.0028,  0.0028],\n",
      "        [-0.0037, -0.0037, -0.0037,  ..., -0.0037, -0.0037, -0.0037]])\n"
     ]
    }
   ],
   "source": [
    "print('Before backward pass: \\n', model[0].weight.grad)\n",
    "\n",
    "loss.backward()\n",
    "\n",
    "print('After backward pass: \\n', model[0].weight.grad)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Training the network!\n",
    "\n",
    "There's one last piece we need to start training, an optimizer that we'll use to update the weights with the gradients. We get these from PyTorch's [`optim` package](https://pytorch.org/docs/stable/optim.html). For example we can use stochastic gradient descent with `optim.SGD`. You can see how to define an optimizer below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [],
   "source": [
    "from torch import optim\n",
    "\n",
    "# Optimizers require the parameters to optimize and a learning rate\n",
    "optimizer = optim.SGD(model.parameters(), lr=0.01)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we know how to use all the individual parts so it's time to see how they work together. Let's consider just one learning step before looping through all the data. The general process with PyTorch:\n",
    "\n",
    "* Make a forward pass through the network \n",
    "* Use the network output to calculate the loss\n",
    "* Perform a backward pass through the network with `loss.backward()` to calculate the gradients\n",
    "* Take a step with the optimizer to update the weights\n",
    "\n",
    "Below I'll go through one training step and print out the weights and gradients so you can see how it changes. Note that I have a line of code `optimizer.zero_grad()`. When you do multiple backwards passes with the same parameters, the gradients are accumulated. This means that you need to zero the gradients on each training pass or you'll retain gradients from previous training batches."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Initial weights -  Parameter containing:\n",
      "tensor([[-0.0330, -0.0122, -0.0081,  ..., -0.0355, -0.0008,  0.0153],\n",
      "        [ 0.0190, -0.0020, -0.0080,  ...,  0.0037, -0.0052,  0.0319],\n",
      "        [ 0.0035, -0.0307,  0.0322,  ..., -0.0027, -0.0132,  0.0339],\n",
      "        ...,\n",
      "        [ 0.0012, -0.0297,  0.0011,  ..., -0.0253,  0.0091, -0.0227],\n",
      "        [ 0.0334, -0.0336,  0.0016,  ...,  0.0140, -0.0346, -0.0119],\n",
      "        [ 0.0203,  0.0062, -0.0123,  ..., -0.0265, -0.0051, -0.0277]],\n",
      "       requires_grad=True)\n",
      "Gradient - tensor([[-0.0007, -0.0007, -0.0007,  ..., -0.0007, -0.0007, -0.0007],\n",
      "        [-0.0016, -0.0016, -0.0016,  ..., -0.0016, -0.0016, -0.0016],\n",
      "        [-0.0020, -0.0020, -0.0020,  ..., -0.0020, -0.0020, -0.0020],\n",
      "        ...,\n",
      "        [ 0.0008,  0.0008,  0.0008,  ...,  0.0008,  0.0008,  0.0008],\n",
      "        [ 0.0002,  0.0002,  0.0002,  ...,  0.0002,  0.0002,  0.0002],\n",
      "        [ 0.0013,  0.0013,  0.0013,  ...,  0.0013,  0.0013,  0.0013]])\n"
     ]
    }
   ],
   "source": [
    "print('Initial weights - ', model[0].weight)\n",
    "\n",
    "images, labels = next(iter(trainloader))\n",
    "images.resize_(64, 784)\n",
    "\n",
    "# Clear the gradients, do this because gradients are accumulated\n",
    "optimizer.zero_grad()\n",
    "\n",
    "# Forward pass, then backward pass, then update weights\n",
    "output = model(images)\n",
    "loss = criterion(output, labels)\n",
    "loss.backward()\n",
    "print('Gradient -', model[0].weight.grad)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Updated weights -  Parameter containing:\n",
      "tensor([[-0.0330, -0.0122, -0.0081,  ..., -0.0355, -0.0008,  0.0154],\n",
      "        [ 0.0191, -0.0020, -0.0080,  ...,  0.0038, -0.0052,  0.0319],\n",
      "        [ 0.0035, -0.0307,  0.0322,  ..., -0.0027, -0.0132,  0.0340],\n",
      "        ...,\n",
      "        [ 0.0012, -0.0297,  0.0011,  ..., -0.0253,  0.0091, -0.0227],\n",
      "        [ 0.0334, -0.0336,  0.0016,  ...,  0.0140, -0.0346, -0.0119],\n",
      "        [ 0.0203,  0.0062, -0.0123,  ..., -0.0265, -0.0051, -0.0277]],\n",
      "       requires_grad=True)\n"
     ]
    }
   ],
   "source": [
    "# Take an update step and few the new weights\n",
    "optimizer.step()\n",
    "print('Updated weights - ', model[0].weight)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Training for real\n",
    "\n",
    "Now we'll put this algorithm into a loop so we can go through all the images. Some nomenclature, one pass through the entire dataset is called an *epoch*. So here we're going to loop through `trainloader` to get our training batches. For each batch, we'll doing a training pass where we calculate the loss, do a backwards pass, and update the weights.\n",
    "\n",
    ">**Exercise:** Implement the training pass for our network. If you implemented it correctly, you should see the training loss drop with each epoch."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Training loss: 1.8584439004662194\n",
      "Training loss: 0.8520682900190861\n",
      "Training loss: 0.5284777172465822\n",
      "Training loss: 0.42789725193591005\n",
      "Training loss: 0.38407350930450823\n",
      "Training loss: 0.3581391613461824\n",
      "Training loss: 0.3403091346944319\n",
      "Training loss: 0.3257950795278239\n",
      "Training loss: 0.3140128951098746\n",
      "Training loss: 0.30349055066831837\n",
      "Training loss: 0.29435332444335605\n",
      "Training loss: 0.286309861973214\n",
      "Training loss: 0.27891139818756566\n",
      "Training loss: 0.271665341405472\n",
      "Training loss: 0.2650324527277494\n",
      "Training loss: 0.2584720215142599\n",
      "Training loss: 0.25260173971020083\n",
      "Training loss: 0.246606529207785\n",
      "Training loss: 0.24056259407274633\n",
      "Training loss: 0.23497576385672922\n",
      "Training loss: 0.22953047929033796\n",
      "Training loss: 0.22380120107042256\n",
      "Training loss: 0.2181983723847279\n",
      "Training loss: 0.2131973893316125\n",
      "Training loss: 0.2081364055058913\n",
      "Training loss: 0.20327885613353777\n",
      "Training loss: 0.1984685780420931\n",
      "Training loss: 0.19398383067837402\n",
      "Training loss: 0.18931673352382203\n",
      "Training loss: 0.18505836494251102\n",
      "Training loss: 0.18086658411029813\n",
      "Training loss: 0.1771521097910938\n",
      "Training loss: 0.17347338400098053\n",
      "Training loss: 0.16976114536828196\n",
      "Training loss: 0.16626981404353816\n",
      "Training loss: 0.16276035223529536\n",
      "Training loss: 0.15917002663477017\n",
      "Training loss: 0.15622321800239433\n",
      "Training loss: 0.1528552137752141\n",
      "Training loss: 0.1497398913899528\n",
      "Training loss: 0.1467131952097866\n",
      "Training loss: 0.14405306926700098\n",
      "Training loss: 0.1411728962604552\n",
      "Training loss: 0.1385341084588057\n",
      "Training loss: 0.13572247343848765\n",
      "Training loss: 0.13321178369938946\n",
      "Training loss: 0.13072302327084262\n",
      "Training loss: 0.12831482674870917\n",
      "Training loss: 0.12579232876473018\n",
      "Training loss: 0.12365882499798783\n"
     ]
    }
   ],
   "source": [
    "## Your solution here\n",
    "\n",
    "model = nn.Sequential(nn.Linear(784, 128),\n",
    "                      nn.ReLU(),\n",
    "                      nn.Linear(128, 64),\n",
    "                      nn.ReLU(),\n",
    "                      nn.Linear(64, 10),\n",
    "                      nn.LogSoftmax(dim=1))\n",
    "\n",
    "criterion = nn.NLLLoss()\n",
    "optimizer = optim.SGD(model.parameters(), lr=0.003)\n",
    "\n",
    "epochs = 50\n",
    "for e in range(epochs):\n",
    "    running_loss = 0\n",
    "    for images, labels in trainloader:\n",
    "        # Flatten MNIST images into a 784 long vector\n",
    "        images = images.view(images.shape[0], -1)\n",
    "    \n",
    "        # TODO: Training pass\n",
    "        # reset gradient\n",
    "        optimizer.zero_grad()\n",
    "        \n",
    "        # feed model with input\n",
    "        output = model(images)\n",
    "        \n",
    "        # calculate the loss\n",
    "        loss = criterion(output, labels)\n",
    "        \n",
    "        # backward propagate the gradient\n",
    "        loss.backward()\n",
    "        \n",
    "        #update the weights\n",
    "        optimizer.step()\n",
    "        \n",
    "        running_loss += loss.item()\n",
    "    else:\n",
    "        print(f\"Training loss: {running_loss/len(trainloader)}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With the network trained, we can check out it's predictions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 53,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAagAAADjCAYAAADQWoDbAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADl0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uIDMuMC4yLCBodHRwOi8vbWF0cGxvdGxpYi5vcmcvOIA7rQAAFMlJREFUeJzt3Xu0XnV95/H3JwlXQVASLJeEeEEWFAfQLIpTbxVoFZWoVQcs7WgdaR2xXqgOHV3VaacuOx2vqzoOo1S8IApeSr0gzAiio1ASQAWiFBGFiCYoBIEquXznj2fTOZ4+DzmHnOz9S/J+rXUWz/ntvZ/9OYfkfM7vt3eeJ1WFJEmtmTd0AEmSxrGgJElNsqAkSU2yoCRJTbKgJElNsqAkSU2yoCRtdUnekuSjQ+d4MJJ8KMl/fZDHPuDXneS6JE+bvm+SJUnuTjL/QYXeTlhQkuZEkhcnWdH9YL0tyReTPGmgLJXkni7L6iTvaPGHfVX9elVdOmb8h1W1R1VtBEhyaZL/0HvAgVlQkrZYktcB7wLeCjwCWAK8D1g+YKwjqmoP4FjgxcDLp++QZEHvqTRjFpSkLZJkL+AvgFdW1aer6p6qWl9V/1BVr59wzHlJfpxkXZLLkvz6lG0nJLk+yc+72c+fduMLk3wuyZ1Jfpbkq0k2+zOsqr4DfBU4vHuem5P8pyTfAu5JsiDJod0s5c5u2e3EaU+zMMnFXaavJDloSt53J7klyV1JViZ58rRjd03yie7Yq5IcMeXYm5McN+b7s7SbBS5I8lfAk4G/7WaEf5vkvUnePu2YC5K8dnPfj22JBSVpSz0R2BX4zCyO+SJwMLAvcBXwsSnbPgj8UVXtyahUvtyNnw7cCixiNEv7z8BmX6styWGMfsBfPWX4ZOBZwN5AgH8ALuryvAr4WJJDpuz/e8BfAguBa6blvRI4Eng4cA5wXpJdp2xfDpw3Zftnk+y0udz3q6o3MirY07plv9OAs4GT7y/oJAuB47rn325YUJK21D7A7VW1YaYHVNVZVfXzqvol8BbgiG4mBrAeOCzJQ6vqjqq6asr4fsBB3Qztq/XALyZ6VZI7GJXPB4C/m7LtPVV1S1X9M3AMsAfwtqq6r6q+DHyOUYnd7/NVdVmX943AE5Ms7r6Wj1bVT6tqQ1W9HdgFmFpuK6vq/KpaD7yDUZkfM9Pv1ThV9Y/AOkbLlwAnAZdW1U+25HlbY0FJ2lI/ZbQENqPrOUnmJ3lbku8luQu4udu0sPvv7wInAD/oltOe2I3/DXAjcFGSm5KcsZlTPb6qHlZVj66qN1XVpinbbpnyeH/glmnbfwAcMG7/qrob+Fl3HEn+NMmqbrnyTmCvKV/L9GM3MZoF7r+Z7DNxNnBK9/gU4CNz8JxNsaAkbalvAL8EnjvD/V/MaNnrOEY/zJd24wGoqiurajmj5bbPAp/sxn9eVadX1aOAE4HXJTmWB2fqzOtHwOJp17OWAKunfL74/gdJ9mC0XPej7nrTG4AXAQ+rqr0ZzWwy4dh5wIHdOR9s3vt9FFjeXdM6lNH3artiQUnaIlW1Dvhz4L1Jnptk9yQ7JXlmkv825pA9GRXaT4HdGd35B0CSnZP8XpK9uiWxu4BN3bZnJ3lMkjAqgY33b9tCVwD3Am/ocj8NeA5w7pR9TkjypCQ7M7oWdXlV3dJ9LRuAtcCCJH8OPHTa8z8hyfO7GeZruq/98llm/AnwqKkDVXUro+tfHwE+1S1XblcsKElbrLv28jrgTYx+WN8CnMb43+o/zGgJbTVwPf/6h/XvAzd3y39/zOgGBRjdVPG/gbsZzdreV1WXzEH2+xgV0jOB2xndHv8H3d1/9zsHeDOjpb0n8P+X1r4EXAjc0H1Nv+BXlw8B/h74d8Ad3df2/K58Z+PdwAuS3JHkPVPGzwYex3a4vAcQ37BQkrZNSZ7CaKnvoM3cMLJNcgYlSdug7lb1VwMf2B7LCSwoSdrmJDkUuJPRbffvGjjOVuMSnySpSb2+DtXx815oG2q7c/Gm87L5vSTNlkt8kqQm+Uq+UuMWLlxYS5cuHTqGNGdWrlx5e1Ut2tx+FpTUuKVLl7JixYqhY0hzJskPZrKfS3ySpCZZUJKkJllQkqQmWVCSpCZZUJKkJllQkqQmWVCSpCZZUJKkJllQkqQmWVBSz5K8Osm1Sa5L8pqh80itsqCkHiU5HHg5cDRwBPDsJI8ZNpXUJgtK6tehwBVVdW9VbQC+Ajx/4ExSkywoqV/XAk9Osk+S3YETgMUDZ5Ka5KuZSz2qqlVJ/hq4CLgHuAbYOH2/JKcCpwIsWbKk14xSK5xBST2rqg9W1ROq6inAHcANY/Y5s6qWVdWyRYs2+7Y50nbJGZTUsyT7VtWaJEsYXX86ZuhMUossKKl/n0qyD7AeeGVV3Tl0IKlFFpTUs6p68tAZpG2B16AkSU2yoCRJTbKgJElNsqAkSU2yoCRJTbKgJElNsqAkSU2yoKSeJXlt915Q1yb5eJJdh84ktciCknqU5ADgT4BlVXU4MB84adhUUpssKKl/C4DdkiwAdgd+NHAeqUkWlNSjqloN/Hfgh8BtwLqqumjYVFKbLCipR0keBiwHHgnsDzwkySlj9js1yYokK9auXdt3TKkJFpTUr+OA71fV2qpaD3wa+LfTd/L9oCQLSurbD4FjkuyeJMCxwKqBM0lNsqCkHlXVFcD5wFXAtxn9HTxz0FBSo3w/KKlnVfVm4M1D55Ba5wxKktSk7XIGNe/Iw8aOv+/vJ6+kHLhgt1mf549veerY8UtueOysn+vB2Pvru0zctvCb944dn7di8uWOWn/fFmeSpLniDEqS1CQLSpLUJAtKktSk7fIalLQ9+fbqdSw94/NDx5C4+W3P6vV8zqAkSU3aLmdQm665fuz4c1b+0cRjVv7Gh2Z9nvcv/sr48y++ZNbP9WDMO3by7xeb2DR2/E9WP2XiMfds2Hvs+NdvePTEY37twp3Gju957uUTj5GkmXAGJfUoySFJrpnycVeS1wydS2rRdjmDklpVVd8FjgRIMh9YDXxm0FBSo5xBScM5FvheVf1g6CBSiywoaTgnAR8fOoTUKgtKGkCSnYETgfMmbP+XNyzceO+6fsNJjbCgpGE8E7iqqn4ybuPUNyycv/tePUeT2rBD3SRxwAu+M3Hb8w94Xi8Z/ukVi8eOb9qlZv1cZz/vfRO37T1vw9jx9xxw2azPw0EXT9y0/riNY8fPOH38C+kCrHznUWPHH3rODnVr+sm4vCc9IGdQUs+SPAQ4ntHbvUuaYIeaQUktqKp7gH2GziG1zhmUJKlJzqCkxj3ugL1Y0fOLdEotcAYlSWpSqmZ/99iDdfy8F/Z3sh3c/MeOf4HXNU/dd+IxR5969djxd+7/1YnHzJvwO86kF6sFeOvtR44dv/yI8S8827qLN52Xrfn8y5YtqxUrVmzNU0i9SrKyqpZtbj9nUJKkJllQkqQmWVCSpCZZUFLPkuyd5Pwk30myKskTh84ktcjbzKX+vRu4sKpe0L1o7O5DB5JaZEFJPUqyF/AU4CUAVXUfcN+QmaRWWVDbqY03fG/s+KLb1kw85uGvvGdrxfkVV/zh+NvM4bpezj+wRwJrgb9LcgSwEnh19/JHkqbwGpTUrwXA44H/UVVHAfcAZ0zfaer7Qa1du7bvjFITLCipX7cCt1bVFd3n5zMqrF8x9f2gFi1a1GtAqRUWlNSjqvoxcEuSQ7qhY4HrB4wkNctrUFL/XgV8rLuD7ybgpQPnkZpkQUk9q6prgM2+Dpm0o7OgdjB1yEETt7150aUTtkxeCV636Rdjx5/+ztdPPGa/lV+fuE2S7uc1KElSkywoSVKTLChJUpMsKElSkywoSVKTLChJUpO8zXwH808v3mPitk1smvXzfXjd48aO7/cObyWXtGUsKKlnSW4Gfg5sBDZUlf9oVxrDgpKG8VtVdfvQIaSWeQ1KktQkC0rqXwEXJVmZ5NShw0itcolP6t+Tqmp1kn2Bi5N8p6oum7pDV1ynAixZsmSIjNLgLKgdzNuf89GhI+zwqmp19981ST4DHA1cNm2fM4EzAZYtW1a9h5Qa4BKf1KMkD0my5/2Pgd8Grh02ldQmZ1BSvx4BfCYJjP7+nVNVFw4bSWqTBSX1qKpuAo4YOoe0LXCJT5LUJAtKktQkC0qS1CSvQe1gFs2/a+gIkjQjzqAkSU2yoCRJTbKgJElNsqCkASSZn+TqJJ8bOovUKgtKGsargVVDh5Ba5l1826l5Rx42dvzX5n/tAY7aZdbned8Xf2fs+KP5xqyfa0eR5EDgWcBfAa8bOI7ULGdQUv/eBbwB2DR0EKllFpTUoyTPBtZU1crN7HdqkhVJVqxdu7andFJbLCipX78JnJjkZuBc4OlJ/tWbdFXVmVW1rKqWLVq0qO+MUhMsKKlHVfVnVXVgVS0FTgK+XFWnDBxLapIFJUlqknfxSQOpqkuBSweOITXLgtpOrTl6r7HjSxbsNuvnuu6+DRO3Pfr13k4uaetwiU+S1CQLSpLUJAtKktQkC0qS1CQLSmrct1evGzqCNAjv4ttObTjhzrHjmx7Ey7+96QfPfYCtt836+SRpJpxBSZKaZEFJPUqya5J/TPLNJNcl+S9DZ5Ja5RKf1K9fAk+vqruT7AR8LckXq+ryoYNJrbGgpB5VVQF3d5/u1H3UcImkdrnEJ/Usyfwk1wBrgIur6oqhM0ktsqCknlXVxqo6EjgQODrJ4dP3mfqGhRvv9TZz7Zhc4tuGLTho8cRtf3bohXN2nsfsOfkdXVfN2Vl2PFV1Z5JLgGcA107bdiZwJsAu+x3sEqB2SM6gpB4lWZRk7+7xbsDxwHeGTSW1yRmU1K/9gLOTzGf0C+Inq+pzA2eSmmRBST2qqm8BRw2dQ9oWuMQnSWqSBSU17nEHjH93ZGl75xLfNmz9/g+fuO15e6yZs/NccO2/mbjtYK6as/NI0lTOoCRJTbKgJElNsqAkSU2yoCRJTbKgpB4lWZzkkiTXd+8H9eqhM0mt8i4+qV8bgNOr6qokewIrk1xcVdcPHUxqjQW1DbvxpN16Oc/uq3bt5Tw7gqq6Dbite/zzJKuAAwALSprGJT5pIEmWMnrZI98PShrDgpIGkGQP4FPAa6rqrjHb/+X9oNaunfx2J9L2zIKSepZkJ0bl9LGq+vS4farqzKpaVlXLFi1a1G9AqREWlNSjJAE+CKyqqncMnUdqmQUl9es3gd8Hnp7kmu7jhKFDSS3yLr5twD8vP3rs+E0vfP/EY9bX7H/3eP2Pf2Ps+EFn3zTxmA2zPsuOraq+BmToHNK2wBmUJKlJFpQkqUkWlCSpSRaUJKlJFpQkqUkWlCSpSd5mvg1YePr3x46vr40Tj9nEprHjP9v4y4nHfPONR40d3/m2Kx8gnSRtHc6gJElNsqCkHiU5K8maJNcOnUVqnQUl9etDwDOGDiFtCywoqUdVdRnws6FzSNsCC0qS1CTv4mvE3S8c/0KtAJ981LsmbNl51udZ/u2XTtz2sAu9W68VSU4FTgVYsmTJwGmkYTiDkhrkGxZKFpQkqVEWlNSjJB8HvgEckuTWJC8bOpPUKq9BST2qqpOHziBtK5xBSZKaZEFJkprkEl8j7jpo/sRtu2b2/5veevuRY8cX/uG6icdMfulZSeqfMyhJUpMsKElSkywoSVKTLChJUpMsKKlnSZ6R5LtJbkxyxtB5pFZ5F18jFn/guonbjj/+RWPHV696xMRjDnnLqrHjG+9cM7tgmlNJ5gPvBY4HbgWuTHJBVV0/bDKpPc6gpH4dDdxYVTdV1X3AucDygTNJTbKgpH4dANwy5fNbuzFJ01hQUoOSnJpkRZIVa9euHTqONAgLSurXamDxlM8P7MZ+he8HJVlQUt+uBA5O8sgkOwMnARcMnElqknfxST2qqg1JTgO+BMwHzqqqybdwSjswC6oRG++c/CKuu/3O+G2P4fuTn2+LE2lrqaovAF8YOofUOpf4JElNsqAkSU2yoCRJTbKgJElNsqAkSU2yoCRJTbKgJElNsqAkSU2yoCRJTbKgJElN8qWOpMatXLny7iTfHTjGQuB2M5hhjjIcNJOdLCipfd+tqmVDBkiywgxm6DtDrwV18abz0uf5JEnbLq9BSZKaZEFJ7Ttz6ACY4X5mGOklQ6qqj/NIkjQrzqAkSU2yoKQGJHlGku8muTHJGWO275LkE932K5IsHSDD65Jcn+RbSf5PkhndKjyXGabs97tJKsmc30k2kwxJXtR9L65Lck7fGZIsSXJJkqu7/x8nbIUMZyVZk+TaCduT5D1dxm8lefxcZ6Cq/PDDjwE/gPnA94BHATsD3wQOm7bPfwTe3z0+CfjEABl+C9i9e/yKITJ0++0JXAZcDiwb4PtwMHA18LDu830HyHAm8Iru8WHAzVvhz+VTgMcD107YfgLwRSDAMcAVc53BGZQ0vKOBG6vqpqq6DzgXWD5tn+XA2d3j84Fjk8zlP9vYbIaquqSq7u0+vRw4cA7PP6MMnb8E/hr4xRyff6YZXg68t6ruAKiqNQNkKOCh3eO9gB/NcQaq6jLgZw+wy3LgwzVyObB3kv3mMoMFJQ3vAOCWKZ/f2o2N3aeqNgDrgH16zjDVyxj99jyXNpuhW0ZaXFWfn+NzzzgD8FjgsUn+b5LLkzxjgAxvAU5JcivwBeBVc5xhJmb7Z2bWfCUJSbOS5BRgGfDUns87D3gH8JI+zzvGAkbLfE9jNIu8LMnjqurOHjOcDHyoqt6e5InAR5IcXlWbesyw1TmDkoa3Glg85fMDu7Gx+yRZwGhZ56c9ZyDJccAbgROr6pdzeP6ZZNgTOBy4NMnNjK57XDDHN0rM5PtwK3BBVa2vqu8DNzAqrD4zvAz4JEBVfQPYldHr4/VpRn9mtoQFJQ3vSuDgJI9MsjOjmyAumLbPBcC/7x6/APhydVeq+8qQ5CjgfzIqp7m+7rLZDFW1rqoWVtXSqlrK6DrYiVW1oq8Mnc8ymj2RZCGjJb+bes7wQ+DYLsOhjApq7RxmmIkLgD/o7uY7BlhXVbfN5Qlc4pMGVlUbkpwGfInRHVxnVdV1Sf4CWFFVFwAfZLSMcyOjC9cnDZDhb4A9gPO6+zN+WFUn9pxhq5phhi8Bv53kemAj8PqqmrPZ7AwznA78rySvZXTDxEvm+BcWknycUREv7K51vRnYqcv4fkbXvk4AbgTuBV46l+cHX0lCktQol/gkSU2yoCRJTbKgJElNsqAkSU2yoCRJTbKgJElNsqAkSU2yoCRJTbKgJElNsqAkSU36fzQudAAkUfFzAAAAAElFTkSuQmCC\n",
      "text/plain": [
       "<Figure size 432x648 with 2 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "%matplotlib inline\n",
    "import helper\n",
    "\n",
    "images, labels = next(iter(trainloader))\n",
    "\n",
    "img = images[0].view(1, 784)\n",
    "# Turn off gradients to speed up this part\n",
    "with torch.no_grad():\n",
    "    logps = model(img)\n",
    "\n",
    "# Output of the network are log-probabilities, need to take exponential for probabilities\n",
    "ps = torch.exp(logps)\n",
    "helper.view_classify(img.view(1, 28, 28), ps)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now our network is brilliant. It can accurately predict the digits in our images. Next up you'll write the code for training a neural network on a more complex dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
